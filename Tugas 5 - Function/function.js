// Soal 1
function teriak() {
    return "Halo Sanbers!"
  }
   
  var tampung = teriak();
  console.log(tampung)
  
  // Soal 2
  var num1 = 12
  var num2 = 4
  function kalikan(num1, num2) {
    return num1*num2
  }
   
  var hasilKali = kalikan(num1, num2)
  console.log(hasilKali)
  
  //Soal 3
  var name = "Husna Afkarina"
  var age = "19"
  var address = "Jl Cisitu Lama"
  var hobby = "Menggambar"
  
  function introduce(name, age, address, hobby) {
    return "Nama saya " + (name) + ", umur saya " + (age) +" tahun, alamat saya di " + (address) + ", dan saya punya hobby yaitu " + (hobby) + "!"
  }
  var perkenalan = introduce(name, age, address, hobby)
  console.log(perkenalan)
  